from scipy.spatial.distance import squareform as sq


def squareform(*args):
  return sq(*args)
