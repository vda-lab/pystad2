# Development instructions

The repository contains the main functionality of STAD as a python / cython package in the `stad` directory, a jupyter-widget backend as python module in `stad/widget`, and a jupyter-widget front-end as typescript / svelte code in the `js` directory.

## Creating a development environment

I recommend using conda for managing development environment. An environment with all dependencies can be created using the `pystad_dev.yml` file.

\* On a windows machine, a compatible c/c++ compiler has to be installed. The [python wiki](https://wiki.python.org/moin/WindowsCompilers) describes which compilers are required by for which python version. Currently, python 3.9, needs the Visual C++ 14.2 compiler. It is part of the [Microsoft Build Tools for Visual Studio 2019](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019) and can be installed as a standalone package if not already present on the system.

\* On a mac machine, make sure that the environment variables in `development/pystad_dev_mac.yml` point to the installation location of OpenMP. Or install OpenMP manually using brew.

## Building the code for development

- To build the python package:
  
  Installing with `pip install -e .` makes python automatically detect changes in the python code. In jupyter-lab, these changes are detected after restarting the kernel.

  Run `python setup.py build_ext --inplace` to build the Cython code. Python detects these changes automatically, jupyter-lab after a kernel restart.

- To build the javascript front-end:
  
  The javascript code is built during installation of the python package. 
  
  Run `jupyter labextension develop . --overwrite` from the root of the repository so jupyter-lab automatically detects new builds of the javascript code. If that command does not work, then new versions of the widget can be installed using `python setup.py install_data`. In both cases, jupyter-lab will detect the new build after a page-refresh in the browser.
  
  To rebuild the javascript code, run `npm run build:dev` from the `js`.

  The `npm run dev` command can be used to check the front-end without starting a jupyter lab instance. This is a legacy option from when installing jupyter widgets required rebuilding jupyter lab itself.

  
## Releasing a new version

- To release a new version of pystad on PyPI:

    Make sure all tests pass:

    - `cd tests`
    - `pytest .`

    Update the version in `setup.cfg` (set release version, remove 'dev'). Update `CHANGELOG.txt`. Commit these changes to git. Run:

    - `python setup.py build_ext --inplace`
    - `python setup.py sdist bdist_wheel`
    - `twine upload --repository testpypi dist/*`
    - `git tag -a X.X.X -m 'a nice comment'`

    \* Wheels are a binary distribution that prevent the installer from having to compile the code themselves. They are more complicated to make so we only distribute wheels for Windows, and maybe MacOS wheels in the future.

    Create a clean conda environment, install pystad from test.pypi and check if all tests pass:
    - `conda create -n test python=3.9 nodejs pytest pywin32`
    - `pip install --index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple pystad`
    - `cd tests`
    - `pytest .`
    - `conda remove -n test --all`

    If everything is ok, remove the package from test.pypi (online interface) and upload to real pypi:
    - `twine upload dist/*`

    Create a clean conda environment, install pystad from pypi and check if all tests pass:

    - `pip install pystad`
    - `pytest --cov`

    Finally, update the version `setup.cfg` (add 'dev0' and increment minor). Commit these changes to git and finnaly run:
    
    - `git push`
    - `git push --tags`

- To release a new version of jupyter_stad on NPM:

    Update `js/package.json` with new npm package version. Also change the version in `stad/widget/widget.py`.

    ```
    # clean out the `dist` and `node_modules` directories
    git clean -fdx
    npm install
    npm publish
    ```