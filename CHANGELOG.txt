v0.2.11
  - Reduce work in gitlab CI pipeline.
  - Add noexcept to avoid GIL in network_distance computation.
v0.2.10
  - Fix Canvas size and device ratio.
v0.2.9
  - Bump minimum widget front-end version so Google Colab picks the newest 
    version.
  - Disable building js front-end on python setup. Assume the front-end is 
    build during packaging (as it is) and skip that work on installation.
v0.2.8
  Small fixes for the widget:
    - Override site styling, text color is always dark
    - Fix titles for edge legends
    - Use 'shift' for selection instead of 's'
    - Fix tooltip positions
v0.2.7
  Add widget support for Jupyter Notebook & Google Colab.
  Published js bundle with npm.
  Reduce pandas dependency for Google Colab.
  Reduce automatic widget resizing.
  Change selection hotkey to 's' (was 'ctrl').
v0.2.6
  Add screenshot functionality to widget.
  Add knn mask & lens for condensed matrices.
  Reduce python dependency to 3.7
v0.2.5
  Fixes for Gitlab CI pipeline.
v0.2.4
  Support selecting 2 groups of nodes in the widget.
v0.2.3
  Small fixes:
    - Removed mutable default values for all functions.
v0.2.2
  Small fixes:
    - stad.validation.network now correctly outputs a sparse matrix that
      only contains non-zero values.
    - disabled console.logs in stad.Widget.
v0.2.1
    Small fixes:
      - Fixed internal _k_min and knn_distance lenses to consistently index
        k from 1. Now, the first closest neighbour is the node itself.
v0.2.0
    Major update:
        - Changed how MST is created when using a Lens: only suppress
          non-adjacent edges. Not using community detection.
        - Added main entry point so STAD can be used from the terminal.
        - Added jupyter-lab widget for interactive visualization of 
          networks.
        - Restructured modules layout and API. 
        - Added boundary coefficient filter function.
        - Added export functions for JSON, GEXF.
        - Updated networkx export function.
        - Updated documentation and tests.
        - Updated example notebooks.
        - Removed vega plot functionality.
        - Removed community detection functionality for now, as it is not
          used internally any more.
v0.1.0
    Major API update, now using data classes to pass output and options
    Implement parallel breadth-first-search for network distances
    Use cython where possible to improve performance
    Add filter functions
v0.0.2
    Fix homepage URL on PyPi
    Add requirements for Binder in project root
v0.0.1
    Initial python implementation of STAD
    Support 1D lenses
    Drawing using networkx and matplotlib
    Example dashboards using Vega / networkx